<?php //include 'core/config.php'; ?>
<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <title>BACuna - Bacolod</title>

	  <!-- Google Font: Source Sans Pro -->
	  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	  <!-- Font Awesome -->
	  <link rel="stylesheet" href="assets/icons/css/all.min.css">
	  <!-- icheck bootstrap -->
	  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
	</head>
	<body class="hold-transition login-page" style="height: 60vh !important; margin-top: 100px;">
	<div class="login-box col-4 offset-4">
	  <!-- /.login-logo -->
	  <div class="card col-8 offset-2">
	    <div class="card-body login-card-body">

		  <div class="login-logo mb-3">
		    <h3 class="m-0"><a href="index.php"><b>BACuna - Bacolod</b></a></h3>
		    <span>Vaccination Information Management System</span>
		  </div>

	      <form id="formLogin" action="#" method="post">
	        <div class="input-group mb-3">
	          <input type="text" class="form-control" placeholder="Username" name="uname" required="">
	          <div class="input-group-append">
	            <div class="input-group-text">
	              <span class="fas fa-user"></span>
	            </div>
	          </div>
	        </div>
	        <div class="input-group mb-3">
	          <input type="password" class="form-control" placeholder="Password" name="pass" required="">
	          <div class="input-group-append">
	            <div class="input-group-text">
	              <span class="fas fa-lock"></span>
	            </div>
	          </div>
	        </div>
	        <div class="row">
	          <!-- /.col -->
	          <div class="col-6 offset-3">
	            <button type="submit" class="btn btn-primary btn-block btn-signin">Sign In</button>
	          </div>
	          <!-- /.col -->
	        </div>
	      </form>
	    </div>
	    <!-- /.login-card-body -->
	  </div>
	</div>
	<!-- /.login-box -->

	<!-- jQuery -->
	<script src="assets/js/jquery-3.5.1.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="assets/js/bootstrap.bundle.min.js"></script>
	</body>
</html>

<script type="text/javascript">
	$("#formLogin").submit( function(e){
		e.preventDefault();
		$(".btn-signin").prop("disabled", true);

		var data = $(this).serialize();
		setTimeout( function(){
			$.ajax({
				type: "POST",
				url: "ajax/auth.php",
				data: data,
				success: function(data){
					if(data == 1){
						window.location="main/index.php";
					}else{
						alert("Error: Username or password incorrect.");
						$(".btn-signin").prop("disabled", false);
						$(".btn-signin").html("Sign in");
					}
				}
			});
		},2000);
	});
</script>