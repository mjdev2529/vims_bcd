<?php
	session_start();
	if($_SESSION['in'] != 1){
		echo "<script>alert('Please login to continue...'); window.location.href='../';</script>";
	}
	include '../config/conn.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>BACuna - Bacolod</title>
</head>
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/icons/css/all.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="../assets/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="../assets/css/fSelect.css">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="../assets/js/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/fSelect.js"></script>
<style type="text/css">
	body{
		padding-top: 5rem;
	}
</style>
<body>
	<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
	  <a class="navbar-brand" href="index.php">BACuna</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="index.php"><u>Home</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="no_email.php"><u>No Email</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="add_new.php"><u>Add New</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="users.php"><u>User Management</u></a>
	      </li>
	    </ul>

	    <ul class="navbar-nav px-3">
		    <li class="nav-item text-nowrap">
		      <a class="nav-link" href="../ajax/logout.php"><i class="fa fa-sign-out-alt"></i> Sign out</a>
		    </li>
		</ul>
	  </div>
	</nav>
	<div class="container col-12 pt-4 mb-3">
		<div class="row">
			<?php if($_SESSION['role'] == 1){?>
				<div class="col-12 mb-3">
					<form id="user_form" method="POST" action="">
						<div class="row">
							<div class="col-2 offset-3 mb-2">
								<label>Username</label>
								<input type="text" class="form-control" name="uname" placeholder="Username">
							</div>
							<div class="col-2 mb-2">
								<label>Password</label>
								<input type="password" class="form-control" name="upass" placeholder="Password">
							</div>

							<div class="col-2 mb-2">
								<label>Role</label>
								<select type="text" class="form-control" name="urole">
									<option value="">Select Role:</option>
									<option value="0">Support</option>
									<option value="1">Admin</option>
								</select>
							</div>
							
							<div class="col-3 pt-2">
								<br>
								<button type="submit" class="btn btn-primary">Add</button>
							</div>
						</div>
					</form>
				</div>
			<?php } ?>
		</div>
		<div class="col-12 mb-3" id="container">
			<table class="table table-bordered table-striped mt-2" id="tbl_users" style="text-align: center;">
		        <thead class="bg-success text-white">
					<tr>
						<th>USERNAME</th>
						<th>ROLE</th>
						<th width="200px">ACTION</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="user_edit_md" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-edit"></i> Edit User</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<div class="row">
				<div class="col-12 mb-3">
					<form id="userEdit_form" method="POST" action="">
						<div class="row">
							<div class="col-8 offset-2 mb-2">
								<label>Password</label>
								<input type="password" class="form-control" name="upass" id="upass" placeholder="Password">
								<input type="hidden" id="uid" name="uid">
							</div>
							<?php if($_SESSION['role'] == 1){?>
								<div class="col-8 offset-2 mb-3">
									<label>Role</label>
									<select type="text" class="form-control" name="urole" id="urole">
										<option value="">Select Role:</option>
										<option value="0">Support</option>
										<option value="1">Admin</option>
									</select>
								</div>
							<?php } ?>
						    <div class="col-4 offset-8">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						    	<button type="submit" class="btn btn-primary">Save</button>
						    </div>
						</div>
					</form>
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>

</body>
</html>
<script type="text/javascript">
	$(document).ready( function(){
		tbl_users();
	});

	function tbl_users(){
		$("#tbl_users").DataTable().destroy();
		$("#tbl_users").DataTable({
	      "processing": true,
	      "ajax":{
	        "type":"POST",
	        "url":"../ajax/datatables/user_data.php",
	        "dataSrc": "data"
	      },
	      "columns": [
	        {"data": "username"},
	        {"data": "role"},
	        {"mRender": function(data,type,row){
	      		return "<button class='btn btn-outline-primary col-5' onclick='user_edit("+row.user_id+")'><i class='fa fa-edit'></i>Edit</button>"+
	      		"<button class='btn btn-outline-danger col-5 offset-1' "+row.hidden+" onclick='user_delete("+row.user_id+")'><i class='fa fa-trash'></i>Delete</button>";
	      	}}
	      ]
		});
	}

	$("#user_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$(".btn").prop("disabled", true);
	    $.ajax({
	        url: "../ajax/user_add.php",
	        data: data,
	        type: 'POST',
	        success: function (data) {
	           if(data == 1){
	           	alert("Success: New user was added.");
	           	$("input").val("");
	           }else{
	           	alert("Error: Something was wrong.");
	           }
				$(".btn").prop("disabled", false);
				tbl_users();
	        }
	    });
	});

	function user_delete(uID){
		var conf = confirm("Are you sure to delete user?");
		if(conf){
			$(".btn").prop("disabled", true);
		    $.ajax({
		        url: "../ajax/user_delete.php",
		        data: {uID: uID},
		        type: 'POST',
		        success: function (data) {
		           if(data == 1){
		           	alert("Success: New user was removed.");
		           	$("input").val("");
		           }else{
		           	alert("Error: Something was wrong.");
		           }
					$(".btn").prop("disabled", false);
					tbl_users();
		        }
		    });
		}
	}

	function user_edit(uID){
		$.ajax({
	        url: "../ajax/user_details.php",
	        data: {uID: uID},
	        type: 'POST',
	        success: function (data) {
	           if(data != ""){
	           	var o = JSON.parse(data);
				$("#user_edit_md").modal();
				$("#uid").val(o.user_id);
				$("#urole").val(o.role);
	           }else{
	           	alert("Error: Something was wrong.");
	           }
	        }
	    });
	}

	$("#userEdit_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$(".btn").prop("disabled", true);
	    $.ajax({
	        url: "../ajax/user_update.php",
	        data: data,
	        type: 'POST',
	        success: function (data) {
	           if(data == 1){
	           	alert("Success: User details was updated.");
	           	$("input").val("");
	           	$("select").val("");
	           }else{
	           	alert("Error: Something was wrong.");
	           }
				$(".btn").prop("disabled", false);
				tbl_users();
				$("#user_edit_md").modal('hide');
	        }
	    });
	});

</script>
