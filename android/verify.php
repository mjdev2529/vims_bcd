<?php

	include '../config/conn.php';
	$qr_data = $_REQUEST["qr_data"];
	$get_master_id = explode("-", $qr_data);
	$vmaster_id = $get_master_id[1];


	$response['data'] = array();

	$query_fetch = mysqli_query($conn,"SELECT * FROM `tbl_vaccination_masterlist` where vmaster_id ='$vmaster_id'");
	
	if(mysqli_num_rows($query_fetch)>0){

		while ($row = mysqli_fetch_array($query_fetch)) {
			$list['vmaster_id'] = $row['vmaster_id'];
			$ml_last_name =  $row['last_name'];
			$ml_first_name = $row['first_name'];
			$contact_no = $row['contact_no'];
			$dob = $row['birthdate'];
			$query_fetch2 = mysqli_query($conn,"SELECT * FROM `tbl_vaccination_vims` where LOWER(last_name) = LOWER('$ml_last_name') and LOWER(first_name) =LOWER('$ml_first_name') and birthday ='$dob' and deferral ='N' order by vaccination_date ASC");
	
			while ($row2 = mysqli_fetch_array($query_fetch2)) {
				$vaccine_name=strtoupper($row2['vaccine_manufacturer_name']);
				$second_dose_scheduled_date='';
				if($vaccine_name=='SINOVAC'){
					$second_dose_scheduled_date = date('M d, Y', strtotime($row2['vaccination_date'] . "+4 week"));
				  }else if($vaccine_name=='ASTRAZENECA'){
					$second_dose_scheduled_date = date('M d, Y', strtotime($row2['vaccination_date'] . "+12 week"));
				  }else if($vaccine_name=='ASTRAZENICA'){
					$second_dose_scheduled_date = date('M d, Y', strtotime($row2['vaccination_date'] . "+12 week"));
				  }else if($vaccine_name=='PFIZER'){
					$second_dose_scheduled_date = date('M d, Y', strtotime($row2['vaccination_date'] . "+3 week"));
				  }else if($vaccine_name=='MODERNA'){
					$second_dose_scheduled_date = date('M d, Y', strtotime($row2['vaccination_date'] . "+4 week"));
				  }else{
					$second_dose_scheduled_date = date('M d, Y', strtotime($row2['vaccination_date']));
					}
	  
				$list['unique_person_id'] = $row2['unique_person_id'];
				$list['fname'] = $row2['first_name'];
				$list['last_name'] = $row2['last_name'];
				$list['contact_no'] = $row2['contact_no'];
				$list['vaccine_manufacturer_name'] =$vaccine_name;
				$list['vaccination_date'] =date('M d, Y', strtotime($row2['vaccination_date']));
				$list['second_dose_scheduled_date'] =	$second_dose_scheduled_date;
				$list['dob'] =$dob;
				array_push($response['data'],$list);
			}
		}
		echo json_encode($response);
	}else{
		echo 0;
	}

	
?>