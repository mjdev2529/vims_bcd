<?php

?>
<!DOCTYPE html>
<html>
<head>
	<title>VACCINATION</title>
</head>
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/icons/css/all.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="../assets/css/select2.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="../assets/js/select2.min.js"></script>
<style type="text/css">
	body{
		padding-top: 5rem;
	}
</style>
<body>
	<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
	  <a class="navbar-brand" href="index.php">Vaccination Information</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  
	</nav>
	<div class="container col-12 pt-4 mb-3">
		<div class="row mb-5">
        <h1 class="col-12 text-center"><p class="font-weight-bold text-danger">Sorry, Error Records or no Records found,<br> Please Contact MITCS - Bacolod (435-4168). <br>Facebook Page: <a href="https://www.facebook.com/BCDGOVTECHSUPPORT" target="_blank">https://www.facebook.com/BCDGOVTECHSUPPORT</a><br>Email: bacunabcdts@gmail.com </p> </h1>
		</div>

    
	</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready( function(){
		$(".select2").select2({
			"width": "100%"
		});
		tbl_schedule(0);
	});


</script>
