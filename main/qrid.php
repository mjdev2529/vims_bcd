<?php
	include '../config/conn.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>VIMS - Bacolod</title>
</head>
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/icons/css/all.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="../assets/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="../assets/css/fSelect.css">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="../assets/js/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/fSelect.js"></script>
<style type="text/css">
	body{
		padding-top: 5rem;
	}
</style>
<body>
	<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
	  <a class="navbar-brand" href="index.php">VIMS</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="index.php"><u>Home</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="center.php"><u>Center</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="qrid.php"><u>QR ID</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="qrid_bulk.php"><u>QR ID Bulk</u></a>
	      </li>
	    </ul>
	  </div>
	</nav>
	<div class="container col-12 pt-4 mb-3">
		<div class="row">
			<div class="col-10">
				<div class="h2">List of Vaccinated</div>
			</div>
			<div class="col-2">
				<button type="button" class="btn btn-outline-primary float-right" onclick="mail_vaccinated()"><i class="fa fa-envelope"></i> Mail Vaccinated</button>
			</div>
			<div class="col-12 mb-3" id="container">
				<table class="table table-bordered table-striped mt-2" id="tbl_vaccinated" style="text-align: center;">
			        <thead class="bg-success text-white">
						<tr>
							<th></th>
							<th>QR UNIQUE ID</th>
							<th>EMAIL ADDRESS</th>
							<th>CATEGORY</th>
							<th>UNIQUE_PERSON_ID</th>
							<th>PWD</th>
							<th>Indigenous Member</th>
							<th>LAST_NAME</th>
							<th>FIRST_NAME</th>
							<th>MIDDLE_NAME</th>
							<th>SUFFIX</th>
							<th>CONTACT_NO</th>
							<th>REGION</th>
							<th>PROVINCE</th>
							<th>MUNI_CITY</th>
							<th>BARANGAY</th>
							<th>SEX</th>
							<th>BIRTHDATE</th>
							<th>VACCINATION_DATE</th>
							<th>VACCINE_MANUFACTURER_NAME</th>
							<th>VACCINATOR_NAME</th>
							<th>1ST_DOSE</th>
							<th>2ND_DOSE</th>
							<th>Encoder Name</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready( function(){
		tbl_vaccinated();
	});

	// function getVaccinatedData(){
	// 	var brgy = $("#brgy").val();
	// 	tbl_vaccinated(brgy);
	// }

	function tbl_vaccinated(brgy){
    	$('#tbl_vaccinated').DataTable().destroy();
	    $('#tbl_vaccinated').DataTable({
	      "scrollX": true,
	      "paging": false,
	      "scrollY": "65vh",
	      "processing": true,
	      "ajax":{
	        "type":"POST",
	        /*"data": {brgy: brgy},*/
	        "url":"../ajax/datatables/vaccinated_data.php",
	        "dataSrc": "data"
	      },
	      "columns": [
	      	{"mRender": function(data,type,row){
	      		return "<button class='btn btn-outline-primary' onclick='generateID("+row.vmaster_id+")'>Generate ID</button><input type='hidden' name='master_id' value='"+row.vmaster_id+"'>";
	      	}},
	        {"data": "qrid"},
	        {"data": "email"},
	        {"data": "category"},
	        {"data": "unique_person_id"},
	        {"data": "pwd_id"},
	        {"data": "indigenous_member"},
	        {"data": "last_name"},
	        {"data": "first_name"},
	        {"data": "middle_name"},
	        {"data": "suffix"},
	        {"data": "contact_number"},
	        {"data": "region"},
	        {"data": "current_residing_province"},
	        {"data": "current_city_municipality"},
	        {"data": "barangay"},
	        {"data": "sex"},
	        {"data": "birthdate"},
	        {"data": "vaccine_manufacturer_name"},
	        {"data": "vaccination_date"},
	        {"data": "vaccinator_name"},
	        {"data": "dose_1"},
	        {"data": "dose_2"},
	        {"data": "encoder_name"}
	      ]
	    });
	  }

	  function generateID(vmID){
	  	// window.location.href="generate_id.php?vmID="+vmID;
	  	window.open(
		  "generate_id.php?vmID="+vmID,
		  '_blank'
		);
	  }

	  function mail_vaccinated(){
	  	var mID = [];
	  	$("input[name=master_id]").each( function(){
	  		mID.push($(this).val());
	  	});
	  	var url = "../ajax/send_mail.php";
	  	$.ajax({
	  		type: "POST",
	  		url: url,
	  		data: {mID: mID},
	  		success: function(data) {
	  			alert(data);
	  		}
	  	});
	  }

</script>
