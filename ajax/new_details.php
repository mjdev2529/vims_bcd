<?php
	if($_POST['vID']){
		include '../config/conn.php';
		
		$vID = $_POST['vID'];

		$details = mysqli_query($conn, "SELECT * FROM tbl_vaccination_vims WHERE vims_id = '$vID'");
		$row = mysqli_fetch_array($details);
		$list = array();
		$list["vmaster_id"] = $row["vims_id"];
    	$list["last_name"] = utf8_decode(utf8_encode($row["last_name"]));
    	$list["first_name"] = utf8_decode(utf8_encode($row["first_name"]));
    	$list["middle_name"] = utf8_decode(utf8_encode($row["middle_name"]));
    	$list["suffix"] = utf8_encode($row["suffix"]);
    	$list["barangay"] = $row["barangay"];
    	$list["birthdate"] = $row["birthday"];

    	$row1 = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_vaccination_vims WHERE vims_id = '$vID'"));
    	$list["vname1"] = $row1["vaccine_manufacturer_name"];
    	$list["lotnum1"] = $row1["lot_number"];
    	$list["vrname1"] = $row1["vaccinator_name"];
    	$list["vdate1"] = $row1["vaccination_date"];
    	$list["dose"] = $row1["dose_1"] == "Y"?"First Dose":($row1["dose_2"] == "Y"?"Second Dose":"Booster Dose");

    	echo json_encode($list);
	}
?>