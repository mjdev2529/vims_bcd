<?php
	session_start();
	if(isset($_SESSION["role"])){
		include '../../config/conn.php';
		$dataView = $_POST["view"];
		if($dataView != 0){
			$fname = isset($_POST['fname']) && $_POST['fname'] != ""?" AND first_name LIKE '%".strtoupper($_POST['fname'])."%'":"";
			$lname = isset($_POST['lname']) && $_POST['lname'] != "" || $_POST['dob'] != ""?" AND last_name LIKE '%".strtoupper($_POST['lname'])."%'":"";
			$dob = isset($_POST['dob']) && $_POST['dob'] != ""?" AND birthday = '".strtoupper($_POST['dob'])."'":"";
			$view3 = $dataView != 3?" GROUP BY first_name, last_name, birthday ORDER BY vims_id":"";
		}

		$viewSql = $dataView == 0?"AND sys_date_encoded BETWEEN '".date("Y-m-1")."' AND '".date("Y-m-t")."' GROUP BY first_name, last_name, birthday ORDER BY vims_id":" $fname $lname $dob $view3";

		$response["data"] = array();
		$users_sql = mysqli_query($conn, "SELECT * FROM tbl_vaccination_vims WHERE timestamp = '-1' $viewSql") or die(mysqli_error($conn));
		while($row = mysqli_fetch_array($users_sql)){

			$fname = strtoupper(mb_substr($row['first_name'], 0, 1, 'utf-8'));
			$lname = strtoupper(mb_substr($row['last_name'], 0, 1, 'utf-8'));
			$mname = strtoupper(mb_substr($row['middle_name'], 0, 1, 'utf-8'));

			$qr_content = $lname.$fname.$mname.date("Ymd", strtotime($row['birthday']))."-".$row['vims_id'];

			$list = array();
		    // $list["oldata_id"] = $row["oldata_id"];
		    $list["vmaster_id"] = $row["vims_id"];
	    	$list["email"] = isset($row["email"])?$row["email"]:"No Email";
	    	$list["unique_person_id"] = "";
		    $list["category"] = $row["category"];
	    	$list["pwd_id"] = "";
	    	$list["indigenous_member"] = "No";
	    	$list["last_name"] = utf8_decode(utf8_encode($row["last_name"]));
	    	$list["first_name"] = utf8_decode(utf8_encode($row["first_name"]));
	    	$list["middle_name"] = utf8_decode(utf8_encode($row["middle_name"]));
	    	$list["suffix"] = utf8_encode($row["suffix"]);
	    	$list["contact_number"] = utf8_encode($row["contact_no"]);
	    	$list["region"] = $row["region"];
	    	$list["current_residing_province"] = $row["province"];
	    	$list["current_city_municipality"] = $row["muni_city"];
	    	$list["barangay"] = $row["barangay"];
	    	$list["sex"] = $row["sex"];
	    	$list["birthdate"] = $row["birthday"];
	    	$list["vaccine_manufacturer_name"] = $row["vaccine_manufacturer_name"];
	    	$list["vaccinator_name"] = $row["vaccinator_name"];
	    	$list["vaccination_date"] = $row["vaccination_date"];
	    	$list["dose_1"] = $row["dose_1"]=="Y"?"YES":"NO";
	    	$list["dose_2"] = $row["dose_2"]=="Y"?"YES":"NO";
	    	$list["dose_booster"] = $row["dose_booster"]=="Y"?"YES":"NO";
	    	$list["dose"] = $row["dose_1"]=="Y"?"FIRST DOSE":($row["dose_2"]=="Y"?"SECOND DOSE":"BOOSTER DOSE");
	    	$list["encoder_name"] = "";
		    $list["qrid"] = $qr_content;

	    	array_push($response['data'],$list);
		}

		echo json_encode($response);
	}
	  
?>