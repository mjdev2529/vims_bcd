<?php
	session_start();
	if($_SESSION['in'] != 1){
		echo "<script>alert('Please login to continue...'); window.location.href='../';</script>";
	}
	include '../config/conn.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>BACuna - Bacolod</title>
</head>
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/icons/css/all.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="../assets/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="../assets/css/fSelect.css">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="../assets/js/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/fSelect.js"></script>
<style type="text/css">
	body{
		padding-top: 5rem;
	}
</style>
<body>
	<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
	  <a class="navbar-brand" href="index.php">BACuna</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="index.php"><u>Home</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="no_email.php"><u>No Email</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="add_new.php"><u>Add New</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="users.php"><u>User Management</u></a>
	      </li>
	    </ul>

	    <ul class="navbar-nav px-3">
		    <li class="nav-item text-nowrap">
		      <a class="nav-link" href="../ajax/logout.php"><i class="fa fa-sign-out-alt"></i> Sign out</a>
		    </li>
		</ul>
	  </div>
	</nav>
	<div class="container col-12 mb-3">
		<div class="row">
			<div class="col-2 offset-3">
				<label>First Name</label><br>
				<input type="text" id="fname" class="form-control" placeholder="First Name" autocomplete="off">
			</div>
			<div class="col-2">
				<label>Last Name</label><br>
				<input type="text" id="lname" class="form-control" placeholder="Last Name" autocomplete="off">
			</div>
			<div class="col-2">
				<label>Date of Birth</label><br>
				<input type="date" id="dob" class="form-control" autocomplete="off">
			</div>
			<div class="col-3 pt-2">
				<br>
				<button class="btn btn-primary btn-search" onclick="tbl_new()"><i class="fa fa-search"></i> Search</button>
			</div>
		</div>
		<div class="btn-group mb-3 col-1 offset-11">
	        <button class="btn btn-sm btn-outline-success" data-toggle="modal" data-target="#add_md">Add</button>
	        <!-- <button class="btn btn-sm btn-outline-danger" onclick="delete_customer()">Delete</button> -->
	    </div>
		<div class="col-12 mb-3" id="container">
			<table class="table table-bordered table-striped mt-2" id="tbl_new" style="text-align: center;">
		        <thead class="bg-success text-white">
					<tr>
						<th>LAST_NAME</th>
						<th>FIRST_NAME</th>
						<th>MIDDLE_NAME</th>
						<th>SUFFIX</th>
						<th>BARANGAY</th>
						<th>BIRTHDATE</th>
						<th width="200px"></th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="add_md" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-xl" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-edit"></i> Add New</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<div class="row">
				<div class="col-12 mb-3">
					<form id="add_form" method="POST" action="" autocomplete="off">
						<div class="row">
							<div class="col-10 offset-1 row mb-3">
								<div class="col-6 mb-2">
									<label>Last Name</label>
									<input type="text" class="form-control" name="lname" placeholder="Last Name" required="">
								</div>
								<div class="col-6 mb-2">
									<label>First Name</label>
									<input type="text" class="form-control" name="fname" placeholder="First Name" required="">
								</div>
								<div class="col-6 mb-2">
									<label>Middle Name</label>
									<input type="text" class="form-control" name="mname" placeholder="Middle Name">
								</div>
								<div class="col-6 mb-2">
									<label>Suffix</label>
									<input type="text" class="form-control" name="suffix" placeholder="Suffix">
								</div>
								<div class="col-6 mb-2">
									<label>Barangay</label>
									<input type="text" class="form-control" name="brgy" placeholder="Barangay" required="">
								</div>
								<div class="col-6 mb-2">
									<label>Birth Date</label>
									<input type="date" class="form-control" name="bdate" placeholder="Birth Date" required="">
								</div>
							</div>

							<div class="col-4">
								<div class="col-10 offset-1 mb-2">
									<label><b>FIRST DOSE</b></label><br>
									<label>Vaccination Date</label>
									<input type="date" class="form-control" name="vdate1" placeholder="Vaccination Date" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccine Name</label>
									<input type="text" class="form-control" name="vname1" placeholder="Vaccine Name" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>LOT Number</label>
									<input type="text" class="form-control" name="lotnum1" placeholder="LOT Number" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccinator Name</label>
									<input type="text" class="form-control" name="vrname1" placeholder="Vaccinator Name" required="">
									<input type="hidden" name="dose_1" value="Y">
								</div>
							</div>


							<div class="col-4">
								<div class="col-10 offset-1 mb-2">
									<label><b>SECOND DOSE</b></label><br>
									<label>Vaccination Date</label>
									<input type="date" class="form-control" name="vdate2" placeholder="Vaccination Date" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccine Name</label>
									<input type="text" class="form-control" name="vname2" placeholder="Vaccine Name" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>LOT Number</label>
									<input type="text" class="form-control" name="lotnum2" placeholder="LOT Number" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccinator Name</label>
									<input type="text" class="form-control" name="vrname2" placeholder="Vaccinator Name" required="">
									<input type="hidden" name="dose_2" value="Y">
								</div>
							</div>

							<div class="col-4">
								<div class="col-10 offset-1 mb-2">
									<label><b>BOOSTER DOSE</b></label><br>
									<label>Vaccination Date</label>
									<input type="date" class="form-control" name="vdate3" placeholder="Vaccination Date">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccine Name</label>
									<input type="text" class="form-control" name="vname3" placeholder="Vaccine Name">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>LOT Number</label>
									<input type="text" class="form-control" name="lotnum3" placeholder="LOT Number">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccinator Name</label>
									<input type="text" class="form-control" name="vrname3" placeholder="Vaccinator Name">
									<input type="hidden" name="dose_booster" value="Y">
								</div>
							</div>
						</div>

						<div class="modal-footer mt-3">
							<div class="offset-10">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						    	<button type="submit" class="btn btn-primary">Save</button>
						    </div>
					    </div>
					</form>
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="edit_md" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-edit"></i> Edit Data</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<div class="row">
				<div class="col-12 mb-3">
					<form id="edit_form" method="POST" action="" autocomplete="off">
						<div class="row">
							<div class="col-6 mb-2">
								<label>Last Name</label>
								<input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" required="">
								<input type="hidden" class="form-control" name="vID" id="vID">
							</div>
							<div class="col-6 mb-2">
								<label>First Name</label>
								<input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" required="">
							</div>
							<div class="col-6 mb-2">
								<label>Middle Name</label>
								<input type="text" class="form-control" name="mname" id="mname" placeholder="Middle Name">
							</div>
							<div class="col-6 mb-2">
								<label>Suffix</label>
								<input type="text" class="form-control" name="suffix" id="suffix" placeholder="Suffix">
							</div>
							<div class="col-6 mb-2">
								<label>Barangay</label>
								<input type="text" class="form-control" name="brgy" id="brgy" placeholder="Barangay" required="">
							</div>
							<div class="col-6 mb-2">
								<label>Birth Date</label>
								<input type="date" class="form-control" name="bdate" id="bdate" placeholder="Birth Date" required="">
							</div>

							<div class="col-6 mt-3">
								<div class="col-10 offset-1 mb-2">
									<label><b>Vaccination Details ( <span id="dose"></span> )</b></label><br>
									<label>Vaccination Date</label>
									<input type="date" class="form-control" name="vdate1" id="vdate1" placeholder="Vaccination Date" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccine Name</label>
									<input type="text" class="form-control" name="vname1" id="vname1" placeholder="Vaccine Name" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>LOT Number</label>
									<input type="text" class="form-control" name="lotnum1" id="lotnum1" placeholder="LOT Number" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccinator Name</label>
									<input type="text" class="form-control" name="vrname1" id="vrname1" placeholder="Vaccinator Name" required="">
									<input type="hidden" name="dose_1" value="Y">
								</div>
							</div>
							
							<div class="col-4 offset-8">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						    	<button type="submit" class="btn btn-primary">Save</button>
						    </div>
						</div>
					</form>
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>

</body>
</html>
<script type="text/javascript">
	$(document).ready( function(){
		tbl_new();
	});

	function tbl_new(){
		
		var fname = $("#fname").val()?$("#fname").val():"";
		var lname = $("#lname").val()?$("#lname").val():"";
		var dob = $("#dob").val()?$("#dob").val():"";

		var view = fname || lname?1:0;

		$("#tbl_new").DataTable().destroy();
		$("#tbl_new").DataTable({
	      "processing": true,
	      "ajax":{
	        "type":"POST",
	        "url":"../ajax/datatables/new_data.php",
	        "dataSrc": "data",
	        "data": {view: view, fname: fname, lname: lname, dob: dob}
	      },
	      "columns": [
	        {"data": "last_name"},
	        {"data": "first_name"},
	        {"data": "middle_name"},
	        {"data": "suffix"},
	        {"data": "barangay"},
	        {"data": "birthdate"},
	        {"mRender": function(data,type,row){
	      		return "<a href='#' class='btn btn-block btn-outline-primary' onclick='vaccinated_details("+row.vmaster_id+")'>View Details</a>"+
	      		"<a href='#' class='btn btn-block btn-outline-success' onclick='generateID("+row.vmaster_id+")'>Generate ID</a>";
	      	}}
	      ]
		});
	}

	$("#add_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$(".btn").prop("disabled", true);
	    $.ajax({
	        url: "../ajax/new_add.php",
	        data: data,
	        type: 'POST',
	        success: function (data) {
	           if(data == 1){
	           	alert("Success: New data was added.");
	           	$("input").val("");
	           	$("#add_md").modal("hide");
	           }else{
	           	alert("Error: Something was wrong.");
	           }
				$(".btn").prop("disabled", false);
				tbl_new();
	        }
	    });
	});

	function user_delete(vID){
		var conf = confirm("Are you sure to delete data?");
		if(conf){
			$(".btn").prop("disabled", true);
		    $.ajax({
		        url: "../ajax/new_delete.php",
		        data: {vID: vID},
		        type: 'POST',
		        success: function (data) {
		           if(data == 1){
		           	alert("Success: Data was removed.");
		           	$("input").val("");
		           }else{
		           	alert("Error: Something was wrong.");
		           }
					$(".btn").prop("disabled", false);
					tbl_new();
		        }
		    });
		}
	}

	function user_edit(vID){
		$.ajax({
	        url: "../ajax/new_details.php",
	        data: {vID: vID},
	        type: 'POST',
	        success: function (data) {
	           if(data != ""){
	           	var o = JSON.parse(data);
				$("#edit_md").modal();
				$("#vID").val(o.vmaster_id);
				$("#lname").val(o.last_name);
				$("#fname").val(o.first_name);
				$("#mname").val(o.middle_name);
				$("#suffix").val(o.suffix);
				$("#brgy").val(o.barangay);
				$("#bdate").val(o.birthdate);

				$("#vname1").val(o.vname1);
				$("#lotnum1").val(o.lotnum1);
				$("#vrname1").val(o.vrname1);
				$("#vdate1").val(o.vdate1);
				$("#dose").html(o.dose);

	           }else{
	           	alert("Error: Something was wrong.");
	           }
	        }
	    });
	}

	$("#edit_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$(".btn").prop("disabled", true);
	    $.ajax({
	        url: "../ajax/new_update.php",
	        data: data,
	        type: 'POST',
	        success: function (data) {
	           if(data == 1){
	           	alert("Success: Data was updated.");
	           	$("input").val("");
	           }else{
	           	alert("Error: Something was wrong.");
	           }
				$(".btn").prop("disabled", false);
				tbl_new();
				$("#edit_md").modal('hide');
	        }
	    });
	});

	function vaccinated_details(vmID){
		window.open('details.php?vmID='+vmID,"_blank");
	}

	function generateID(vmID){
		window.open(
			"generate_id.php?vmID="+vmID,
			'_blank'
		);
	}

</script>
