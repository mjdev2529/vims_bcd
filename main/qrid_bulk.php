<?php
	include '../config/conn.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>VIMS - Bacolod</title>
</head>
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/icons/css/all.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="../assets/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="../assets/css/fSelect.css">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="../assets/js/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/fSelect.js"></script>
<style type="text/css">
	body{
		padding-top: 5rem;
	}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<!-- <script>
$(function(){
  $.fn.qrcode = function(text,width,height) { 
    return "http://chart.apis.google.com/chart?cht=qr&chld=L|0&chl="+ text +"&chs="+ width +"x"+ height;
  }
  $('#generate').click(function(){
    var chart = $().qrcode($('#text').val(),$('#width').val(),$('#height').val());
    $("#qrcode_image").html("<img src='"+chart+"'>");
  });
})
</script> -->
<body>
	<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
	  <a class="navbar-brand" href="index.php">Vaccination</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="index.php"><u>Home</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="center.php"><u>Center</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="qrid.php"><u>QR ID</u></a>
	      </li>
	    </ul>
	  </div>
	</nav>
	<div class="container col-12 pt-4 mb-3">
		<div class="row">
			<?php
				$get_sched = mysqli_query($conn,"SELECT * FROM tbl_vaccination_masterlist a INNER JOIN tbl_vaccination_records b ON a.vmaster_id = b.vmaster_id");
				while($row = mysqli_fetch_array($get_sched)){
					$fname = strtoupper(mb_substr($row['first_name'], 0, 1, 'utf-8'));
					$lname = strtoupper(mb_substr($row['last_name'], 0, 1, 'utf-8'));
					$mname = strtoupper(mb_substr($row['middle_name'], 0, 1, 'utf-8'));

					$qr_content = $lname.$fname.$mname.date("Ymd", strtotime($row['birthdate']))."-".$row['vmaster_id']."\n";
			?>
				<div class="card col-3 p-2 pb-2">
					<!-- <?=$qr_content?> -->
					<img class="" src="assets/src/card.jpg">
					<img width="100" height="100" src="http://chart.apis.google.com/chart?cht=qr&chld=L|0&chl=<?=$qr_content?>&chs=150x150" style="margin-top: -110px;margin-left: 326px;">
				</div>
			<?php } ?>
			<!-- <div class="card col-4">
				test
			</div>
			<div class="card col-4">
				test
			</div> -->
		</div>
	</div>
</body>
</html>
<!-- <script type="text/javascript">
	$(document).ready( function(){
		$(".fSelect").fSelect({
    		placeholder: 'Select Barangay:',
		});
		$(".select2").select2({
			"width": "100%"
		});
	});

	$("#center_form").submit( function(e){
		e.preventDefault();
		var max = $("input[name=center_max]").val();
		var division = (max*1)/4;
		var data = $(this).serialize()+"&div="+division;
		$(".btn").prop("disabled", true);
	    $.ajax({
	        url: "ajax/center_add.php",
	        data: data,
	        type: 'POST',
	        success: function (data) {
	           if(data != 0){
	           	alert("Success: New center was added.");
	           	$("input").val("");
	           	$("fSelect").fSelect("reload");
	           }else{
	           	alert("Error: Something was wrong.");
	           }
				$(".btn").prop("disabled", false);
	        }
	    });
	});

</script> -->
