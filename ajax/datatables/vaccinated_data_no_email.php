<?php
	include '../../config/conn.php';
	ini_set("max_execution_time", 0);

	$fname = isset($_POST['fname']) && $_POST['fname'] != ""?" AND first_name LIKE '%".strtoupper($_POST['fname'])."%'":"";
	$lname = isset($_POST['lname']) && $_POST['lname'] != "" || $_POST['dob'] != ""?" AND last_name LIKE '%".strtoupper($_POST['lname'])."%'":"";
	$dob = isset($_POST['dob']) && $_POST['dob'] != ""?" AND birthday = '".strtoupper($_POST['dob'])."'":"";

	if($fname || $lname || $dob){
		$response["data"] = array();
		$vims_sql = mysqli_query($conn, 
			"SELECT * FROM (
			SELECT * FROM tbl_vaccination_vims WHERE dose_1 = 'Y'
			UNION
			SELECT * FROM tbl_vaccination_vims WHERE dose_2 = 'Y'
			) AS vims 
			WHERE /*(first_name, last_name) NOT IN (SELECT first_name, last_name FROM tbl_vaccination_mailer) AND*/ dose_2 = 'Y' $fname $lname $dob GROUP BY first_name, last_name, birthday ORDER BY last_name") or die(mysqli_error($conn));
		if(mysqli_num_rows($vims_sql) != 0){
			while($row = mysqli_fetch_array($vims_sql)){

				$fname = strtoupper(mb_substr($row['first_name'], 0, 1, 'utf-8'));
				$lname = strtoupper(mb_substr($row['last_name'], 0, 1, 'utf-8'));
				$mname = strtoupper(mb_substr($row['middle_name'], 0, 1, 'utf-8'));

				$qr_content = $lname.$fname.$mname.date("Ymd", strtotime($row['birthday']))."-".$row['vims_id'];

				$data = mysqli_num_rows(mysqli_query($conn,"SELECT * FROM tbl_vaccination_mailer WHERE first_name = '$row[first_name]' AND last_name = '$row[last_name]'"));

				$list = array();
			    // $list["oldata_id"] = $row["oldata_id"];
			    $list["vmaster_id"] = $row["vims_id"];
		    	// $list["email"] = isset($row["email"])?$row["email"]:"No Email";
		    	$list["unique_person_id"] = "";
			    $list["category"] = $row["category"];
		    	$list["pwd_id"] = "";
		    	$list["indigenous_member"] = "No";
		    	$list["last_name"] = utf8_encode($row["last_name"]);
		    	$list["first_name"] = utf8_encode($row["first_name"]);
		    	$list["middle_name"] = utf8_encode($row["middle_name"]);
		    	$list["suffix"] = utf8_encode($row["suffix"]);
		    	$list["contact_number"] = utf8_encode($row["contact_no"]);
		    	$list["region"] = $row["region"];
		    	$list["current_residing_province"] = $row["province"];
		    	$list["current_city_municipality"] = $row["muni_city"];
		    	$list["barangay"] = $row["barangay"];
		    	$list["sex"] = $row["sex"];
		    	$list["birthdate"] = $row["birthday"];
		    	$list["vaccine_manufacturer_name"] = $row["vaccine_manufacturer_name"];
		    	$list["vaccinator_name"] = $row["vaccinator_name"];
		    	$list["vaccination_date"] = $row["vaccination_date"];
		    	$list["dose_1"] = $row["dose_1"]=="Y"?"YES":"NO";
		    	$list["dose_2"] = $row["dose_2"]=="Y"?"YES":"NO";
		    	$list["encoder_name"] = "";
		    	$list["qrid"] = $qr_content;
		    	$list["is_generated"] = $data;
		    	// $list["is_exported"] = $row["is_exported"];

		    	array_push($response['data'],$list);
			}
				echo json_encode($response);

		}else{
			$response["data"] = array();
			echo json_encode($response);
		}
	}
	  
?>