<?php
	session_start();
	if($_SESSION['in'] != 1){
		echo "<script>alert('Please login to continue...'); window.location.href='../';</script>";
	}
	include '../config/conn.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>BACuna - Bacolod</title>
</head>
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/icons/css/all.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="../assets/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="../assets/css/fSelect.css">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="../assets/js/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/fSelect.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<style type="text/css">
	body, html{
		width: 100vw;
		/*zoom: 0.5;*/
	}

	/*@media print{
		.id_body{
			background-image: url(../assets/src/card2.jpg);
		    background-size: cover;
		    background-position: center;
		    background-repeat: no-repeat;
		    width: 445px;
		    height: 300px;
		    font-size: small;
		    font-family: arial, sans-serif;
		}
	}*/
	.id_body{
		background-image: url(../assets/src/card5.jpg);
	    background-size: cover;
	    background-position: center;
	    background-repeat: no-repeat;
	    width: 910px;
	    height: 620px;
	    font-size: x-large;
	    font-family: arial, sans-serif;
	}
	table, td{
		border: none !important;
		font-weight: bold;
		border: 1px solid;
	}
</style>
<body>
	<div class="row">
		<?php

			$vmID = $_GET["vmID"];
			$get_sched = mysqli_query($conn,"SELECT * FROM tbl_vaccination_vims WHERE vims_id = '$vmID'");
			$row = mysqli_fetch_array($get_sched);

			if($row){

		?>
		<div class="col-12 mt-3 ml-3">
			<button type="button" class="btn btn-outline-success" onclick="CreatePDFfromHTML()">Download</button>
		</div>
		<?php
			$fname = strtoupper(mb_substr($row['first_name'], 0, 1, 'utf-8'));
			$lname = strtoupper(mb_substr($row['last_name'], 0, 1, 'utf-8'));
			$mname = strtoupper(mb_substr($row['middle_name'], 0, 1, 'utf-8'));
			$suffix = $row['suffix'] != 'NA' && $row['suffix'] != 'NONE'?$row['suffix']:"";
			$m_name = $mname?$mname.".":"";

			$qr_content = $lname.$fname.$mname.date("Ymd", strtotime($row['birthday']))."-".$row['vims_id'];
			$qr_data = "https://vims.bacolodcity.gov.ph/android/QR_verify.php?qr_data=".base64_encode($qr_content);
			$brgy = explode("_", $row['barangay']);
			$brgy_name = isset($brgy[3])?$brgy[3]:"";

			//INFO
			$person_fname = $row["first_name"];
			$person_lname = $row["last_name"];
			$person_bday = $row["birthday"];

			//VIMS DATA
			$vData1 = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM tbl_vaccination_vims WHERE UPPER(first_name) = '$person_fname' AND UPPER(last_name) = '$person_lname' AND birthday = '$person_bday' AND dose_1 = 'Y'"));

			$vData2 = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM tbl_vaccination_vims WHERE UPPER(first_name) = '$person_fname' AND UPPER(last_name) = '$person_lname' AND birthday = '$person_bday' AND dose_2 = 'Y'"));

			//DOSE 1
			// $dose1 = $vData1["dose_1"] == "Y"?"1st Dose":"";
			$date1 = date("Y-m-d", strtotime($vData1["vaccination_date"]));
			$vaccinator1 = strtoupper($vData1["vaccinator_name"]);
			$vcr = explode(",", $vaccinator1);
			$vcr_1 = isset($vcr[1])?$vcr[1]:"";
			$lotNo = $vData1["lot_number"];
			$vaccine = $vData1["vaccine_manufacturer_name"];

			//DOSE 2
			// $dose2 = $vData2["dose_2"] == "Y"?"2nd Dose":"";
			$date2 = date("Y-m-d", strtotime($vData2["vaccination_date"]));
			$vaccinator2 = strtoupper($vData2["vaccinator_name"]);
			$vcr2 = explode(",", $vaccinator2);
			$vcr_2 = isset($vcr2[1])?$vcr2[1]:"";
			$lotNo2 = $vData2["lot_number"];
			$vaccine2 = $vData2["vaccine_manufacturer_name"];
		?>
			<div class="row">
				<div id="DivIdToPrint" class="id_body mt-3 ml-5 container">
					<div class="row">
						<!-- <div class="col-12 pt-4"></div> -->
						<div class="col-9 pt-4 text-center" style="margin-top: 8%">
							<div class="m-0 mt-1"><b><?=strtoupper($row['first_name'])." ".$m_name." ".strtoupper($row['last_name'])." ".$suffix?></b></div>
							<div class="m-0"><b>DOB: <?=date("F j, Y", strtotime($row['birthday']))?></b></div>
							<div class="m-0 mt-3"><b>BRGY. <?=$row['barangay']?></b></div><br>
						</div>
						<div class="col-8 pl-4 pt-3">
    						<table class="text-center" style="width: 85%; margin-top: 18%; margin-left: 21%; font-size: x-large;">
								<tbody style="height: 8rem !important;">
									<tr>
										<td style="width: 25%;"><small><b><?=$date1?></b></small></td>
										<td style="width: 25%; font-size: xx-small;" class="p-0 pt-1"><b><?=$vcr[0].",<br>".$vcr_1?></b></td>
										<td style="width: 25%; font-size: medium;"><small><b><?=$lotNo?></small></b></td>
										<td style="width: 25%; font-size: medium;"><small><b><?=$vaccine?></b></small></td>
									</tr>
									<tr>
										<td style="width: 25%;"><small><b><?=$date2?></b></small></td>
										<td style="width: 25%; font-size: xx-small;" class="p-0 pt-1"><b><?=$vcr2[0].",<br>".$vcr_2?></b></td>
										<td style="width: 25%; font-size: medium;"><small><b><?=$lotNo2?></b></small></td>
										<td style="width: 25%; font-size: medium;"><small><b><?=$vaccine2?></b></small></td>
									</tr>
								</tbody>
    						</table>
						</div>
						<div class="col-4">
							<img crossOrigin="anonymous" width="210" id="qrcode" height="210" style="margin-top: 30%;margin-left: 17%;" src="http://chart.apis.google.com/chart?cht=qr&chld=L|0&chl=<?=$qr_data?>&chs=150x150">
						</div>
					</div>
				</div>
			</div>
		<?php
			}else{
		 ?>
		 	<div class="col-12 pt-5">
		 		<h1 class="text-center"><i class="fa fa-info-circle"></i> No Data Available.</h1>
		 	</div>
		 <?php
		 	} 
		 ?>
	</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready( function(){
	});

    function getBase64Image(imgUrl, callback) {

	    var img = new Image();

	    // onload fires when the image is fully loadded, and has width and height

	    var canvas = document.createElement("canvas");
	    var ctx = canvas.getContext("2d", {alpha:false});

	    img.onload = function(){
	      canvas.width = img.naturalWidth;
	      canvas.height = img.naturalHeight;
	      ctx.drawImage(
		    img,
		    0, 0, img.naturalWidth, img.naturalHeight,
		    0, 0, canvas.width, canvas.height
		  );
	      var dataURL = canvas.toDataURL("image/png", 1.0);//,
	          // dataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

	      callback(dataURL); // the base64 string

	    };

	    // set attributes and src 
	    img.setAttribute('crossOrigin', 'anonymous'); //
	    img.src = imgUrl;

	}

	function CreatePDFfromHTML() {
        var HTML_Width = $("#DivIdToPrint").width();
        var HTML_Height = $("#DivIdToPrint").height();
        var top_left_margin = 15;
        var PDF_Width = HTML_Width + (top_left_margin * 2);
        var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
        var canvas_image_width = 375;//HTML_Width;
        var canvas_image_height = 280;//HTML_Height;

        var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;	

        html2canvas($("#DivIdToPrint")[0], {useCORS: true,scale: 2}).then(function (canvas) {
		    var qrcode = document.getElementById('qrcode');
		    this.getBase64Image(qrcode.src, function(base64image){ 	
				$("#qrcode").attr('src', base64image);
			});

            var imgData = canvas.toDataURL("image/jpeg", 1.0);
            var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
            pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
   //  		alert("BACuna Card Generated.");
			// pdf.save("<?=$qr_content?>.pdf");

		     $.ajax({
		     	url: "../ajax/mail_sent.php",
                type: 'POST',
                data: {mID: "<?=$vmID?>"},
                success: function(data){alert(data)
            		pdf.save("<?=$qr_content?>.pdf");
				},
                error: function(data){alert(data)}
            });
        });

    }

</script>
