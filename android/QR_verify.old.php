<?php
	include '../config/conn.php';

	$qr_data = $_REQUEST["qr_data"];
  $qr_content = base64_decode($qr_data);
	$get_master_id = explode("-", $qr_content);
	$vmaster_id = $get_master_id[1];

    //personal data
    $fname = '';
    $last_name ='';
    $vaccine_manufacturer_name='';
    $vaccination_date='';

	$response['data'] = array();

	$query_fetch = mysqli_query($conn,"SELECT * FROM `tbl_vaccination_masterlist` where vmaster_id ='$vmaster_id'");
	
	if(mysqli_num_rows($query_fetch)>0){

		while ($row = mysqli_fetch_array($query_fetch)) {
			$list['vmaster_id'] = $row['vmaster_id'];
			$ml_last_name =  $row['last_name'];
			$ml_first_name = $row['first_name'];
			$contact_no = $row['contact_no'];
			$dob = $row['birthdate'];
			$query_fetch2 = mysqli_query($conn,"SELECT * FROM `tbl_vaccination_vims` where LOWER(last_name) = LOWER('$ml_last_name') and LOWER(first_name) =LOWER('$ml_first_name') and birthday ='$dob' and deferral ='N' group by dose_2 order by vaccination_date ASC");
	
			while ($row2 = mysqli_fetch_array($query_fetch2)) {
				$list['unique_person_id'] = $row2['unique_person_id'];
				$list['fname'] = $row2['first_name'];
				$list['last_name'] = $row2['last_name'];
				$list['contact_no'] = $row2['contact_no'];
				$list['vaccine_manufacturer_name'] = $row2['vaccine_manufacturer_name'];
				$list['vaccination_date'] = $row2['vaccination_date'];
				$list['dose_1'] =$row2['dose_1'];
                $list['dose_2'] =$row2['dose_2'];
                $list['birthdate'] =$dob;
				array_push($response['data'],$list);
			}
		}
        $dose_records_count =  count($response['data']);
        if( $dose_records_count==1){
          $dose_data =$response['data'][0];
          $dose_1_data =  $dose_data['dose_1'];
          $dose_2_data =  $dose_data['dose_2'];
          $vaccine_name = strtoupper($dose_data['vaccine_manufacturer_name']);
          if($dose_1_data=='Y'&& $dose_2_data='N'){ // clean record
            $first_name = $dose_data['fname'];
            $last_name = $dose_data['last_name'];
            $birthdate = $dose_data['birthdate'];
         
            $vaccination_date =   date('M d, Y',strtotime($dose_data['vaccination_date']));
            $final_vaccination_date = $dose_data['vaccination_date'];

            if($vaccine_name=='SINOVAC'){
              $second_dose_scheduled_date = date('M d, Y', strtotime($vaccination_date . "+4 week"));
              }else if($vaccine_name=='ASTRAZENECA'){
              $second_dose_scheduled_date = date('M d, Y', strtotime($vaccination_date . "+12 week"));
              }else if($vaccine_name=='ASTRAZENICA'){
                $second_dose_scheduled_date = date('M d, Y', strtotime($vaccination_date . "+12 week"));
                }else if($vaccine_name=='PFIZER'){
              $second_dose_scheduled_date = date('M d, Y', strtotime($vaccination_date . "+3 week"));
              }else if($vaccine_name=='MODERNA'){
              $second_dose_scheduled_date = date('M d, Y', strtotime($vaccination_date . "+4 week"));
              }else{
              $second_dose_scheduled_date = date('M d, Y', strtotime($vaccination_date));
              }

          }else if($dose_1_data=='N'&& $dose_2_data='Y'){ // bad record

            $first_name = $dose_data['fname'];
            $last_name = $dose_data['last_name'];
            $birthdate = $dose_data['birthdate'];
            $vaccination_date = $dose_data['vaccination_date'];

          }else if($dose_1_data=='Y'&& $dose_2_data='Y'){ // bad record
            header("Location: no_records_found.php");
	
          }else if($dose_1_data=='N'&& $dose_2_data='N'){ // bad record
            header("Location: no_records_found.php");
	
          }
  
        }else if ($dose_records_count>1){
            $first_dose =$response['data'][0];
            $second_dose = $response['data'][1];

            $dose_1_data =  $first_dose['dose_1'];
            $dose_2_data =  $second_dose['dose_2'];

            $first_name = $first_dose['fname'];
            $last_name = $first_dose['last_name'];
            $birthdate = $first_dose['birthdate'];

            if($dose_1_data=='Y'&& $dose_2_data='Y'){ // completed vacc
              
                $first_dose_vaccine_name = $first_dose['vaccine_manufacturer_name'];
                $second_dose_vaccine_name = $second_dose['vaccine_manufacturer_name'];
                $first_vaccination_date =   date('M d, Y',strtotime($first_dose['vaccination_date']));
                $second_vaccination_date =   date('M d, Y',strtotime($second_dose['vaccination_date']));

            }else if($dose_1_data=='Y'&& $dose_2_data='N'){ // bad record
              header("Location: no_records_found.php");
            }else if($dose_1_data=='N'&& $dose_2_data='Y'){ // bad record
                header("Location: no_records_found.php");
	
            }else if($dose_1_data=='N'&& $dose_2_data='N'){ // bad record
                header("Location: no_records_found.php");
            }

        }else{
            header("Location: no_records_found.php");
	
        }
		
   
	}else{

        header("Location: no_records_found.php");
	
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>VACCINATION</title>
</head>
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/icons/css/all.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="../assets/css/select2.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="../assets/js/select2.min.js"></script>
<style type="text/css">
	body{
		padding-top: 5rem;
	}
  .border-10{
border-style:solid;
 border-width: 5px;
 border-color: #E6AD4A;
 border-radius: 20px;
}
.bordergreen-10{
border-style:solid;
 border-width: 5px;
 border-color: #079205;
 border-radius: 20px;
}
}
</style>
<body  style="
      background-image: url('./bg_image.jpg');background-repeat: no-repeat;background-size: 140% 250%;
    ">
	<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
	  <a class="navbar-brand" href="index.php">Vaccination Information</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  
	</nav>
  <div style='text-align: center;'><img src="logo.png"  width="150" height="150" /></div>
<div class="mt-3">    <h1 class="col-12 text-center">Bacolod City Government</h1></div>
<div class="mt-3">     <h1 class="col-12 text-center"><span style="color:green">Covid-19 Vaccination Verifier</span></h1></div>
	<div class="container col-10 pt-4 mb-3" style="  background-color: #ffffff;
  opacity: 0.6;padding: 30px;margin:50;  border-radius: 50px;margin-top:50px">
		<div class="row mb-5">
	
        
            <?php if( $dose_records_count==1&$dose_2_data=='Y'){
                ?>    
            		<h1 class="col-12 text-center font-weight-bold"><?php echo  $first_name.' '. $last_name;?></h1>
			<h3 class="col-12 text-center  mb-5">Date Of Birth: <?php echo $birthdate;?></h3>
                <span class="col-2" ></span>
                <span  class="col-8  bordergreen-10">
                <h1 class="col-12 text-center"> Vaccine </h1>
                <h1 class="col-12 text-center"><p class="font-weight-bold text-success"><?php echo  $vaccine_name;?></p> </h1>
                <h3 class="col-12 text-center">Date Vaccinated: <?php echo  $vaccination_date;?> </h3>
                </span>
                <span class="col-2" ></span>
               <?php

            }else if( $dose_records_count>1&$dose_1_data=='Y'&$dose_2_data=='Y'){
                ?>
                		<h1 class="col-12 text-center font-weight-bold"><?php echo  $first_name.' '. $last_name;?></h1>
			<h3 class="col-12 text-center  mb-5">Date Of Birth: <?php echo $birthdate;?></h3>
      <h1 class="col-12 text-center"><span style="color:green">VACCINATION COMPLETE</span></h1>
                <span class="col-2" ></span><span  class="col-8  bordergreen-10">
                <h1 class="col-12 text-center">1st Dose Vaccine </h1>
                <h1 class="col-12 text-center"><p class="font-weight-bold text-success"><?php echo   $first_dose_vaccine_name;?></p> </h1>
                <h3 class="col-12 text-center">Date Vaccinated: <?php echo  $first_vaccination_date;?> </h3>
                </span>
                <span class="col-2" ></span>
            </div>
    
            <div class="row">
                <span class="col-2" ></span>
                <span  class="col-8  bordergreen-10">
                <h1 class="col-12 text-center">2nd Dose Vaccine </h1>
                <h1 class="col-12 text-center"><p class="font-weight-bold text-success"><?php echo  $second_dose_vaccine_name;?></p> </h1>
                <h3 class="col-12 text-center">Date Vaccinated: <?php echo  $second_vaccination_date;?> </h3>
                </span>
                <span class="col-2" ></span>
                </div><?php
            }else {?><?php
              ?>
              		<h1 class="col-12 text-center font-weight-bold"><?php echo  $first_name.' '. $last_name;?></h1>
			<h3 class="col-12 text-center  mb-5">Date Of Birth: <?php echo $birthdate;?></h3><span class="col-2" ></span>
      
      <span  class="col-8  bordergreen-10">
              <h1 class="col-12 text-center">1st Dose Vaccine </h1>
              <h1 class="col-12 text-center"><p class="font-weight-bold text-success"><?php echo  $vaccine_name;?></p> </h1>
              <h3 class="col-12 text-center">Date Vaccinated: <?php echo  $vaccination_date;?> </h3>
              </span>

             
              <span class="col-2" ></span>

          </div>


          
          <div class="row">
               
              <span class="col-2"></span>
              <span  class="col-8  bordergreen-10">
              <h1 class="col-12 text-center">Scheduled Vaccine </h1>
              <h3 class="col-12 text-center">Date: <?php echo  $second_dose_scheduled_date ;?> </h3>
              </span>
              <span class="col-2" ></span>
                </div>
  
          <?php
          }?>
			
	</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready( function(){
	
	});


</script>
