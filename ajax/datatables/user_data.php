<?php
	session_start();
	if(isset($_SESSION["role"])){
		include '../../config/conn.php';

		$ufilter = $_SESSION["role"] == 1?"":" WHERE user_id = '$_SESSION[uid]'";

		$response["data"] = array();
		$users_sql = mysqli_query($conn, "SELECT * FROM tbl_vaccination_users $ufilter") or die(mysqli_error($conn));
		while($row = mysqli_fetch_array($users_sql)){

			$list = array();
		    $list["user_id"] = $row["user_id"];
	    	$list["username"] = $row["username"];
		    $list["password"] = $row["password"];
	    	$list["role"] = $row["role"] == 0?"Support":"Admin";
	    	$list["hidden"] = $_SESSION["role"] == 1?"":"style='display:none;'";

	    	array_push($response['data'],$list);
		}

		echo json_encode($response);
	}
	  
?>