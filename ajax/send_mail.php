<?php
	include '../config/conn.php';
	
	$mID = $_POST["mID"];
	$genID = base64_encode($mID);
	$date = date("Y-m-d");

	//Import PHPMailer classes into the global namespace
	//These must be at the top of your script, not inside a function
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;

	/* Exception class. */
	require '../assets\PHPMailer\src\Exception.php';

	/* The main PHPMailer class. */
	require '../assets\PHPMailer\src\PHPMailer.php';

	/* SMTP class, needed if you want to use SMTP. */
	require '../assets\PHPMailer\src\SMTP.php';

	//Create an instance; passing `true` enables exceptions
	$mail = new PHPMailer(true);

	$email = mysqli_fetch_array(mysqli_query($conn,"SELECT email FROM tbl_vaccination_eoc WHERE vac_id = '$mID'"));
	$add = true;//mysqli_query($conn, "INSERT INTO tbl_vaccination_mailer SET vmaster_id = '$mID', date_added = '$date'");
	if($add){
		try {
	       /* SMTP parameters. */

		   /* Tells PHPMailer to use SMTP. */
		   $mail->isSMTP();
		   
		   /* SMTP server address. */
		   $mail->Host = 'smtp-mail.outlook.com';

		   /* Use SMTP authentication. */
		   $mail->SMTPAuth = TRUE;
		   
		   /* Set the encryption system. */
		   $mail->SMTPSecure = 'tls';
		   
		   /* SMTP authentication username. */
		   $mail->Username = 'ict@bacolodcity.gov.ph';
		   
		   /* SMTP authentication password. */
		   $mail->Password = 'BcdMitcs6100';
		   
		   /* Set the SMTP port. */
		   $mail->Port = 587;

		    //Recipients
		    $mail->setFrom('ict@bacolodcity.gov.ph', 'EOC - Bacolod');
		    // $mail->addAddress('mjdev2529@gmail.com');     //Add a recipient
		    $mail->addAddress($email[0]);               //Name is optional
		    // $mail->addReplyTo('ict@bacolodcity.gov.ph', 'Information');
		    // $mail->addCC('cc@example.com');
		    // $mail->addBCC('bcc@example.com');

		    //Content
			    $mail->isHTML(true);                                  //Set email format to HTML
			    $mail->Subject = 'BACuna Vaccination Card';
			    $mail->AddEmbeddedImage("../assets/src/eoc.png", "eoc");
			    $mail->AddEmbeddedImage("../assets/src/covac.jpg", "covac");
			    $mail->Body    = 'Good day,<br><br>Please generate your BACuna vaccination ID card through this link: https://vims.bacolodcity.gov.ph/main/bac_una_id_v2.php?vmID='.$genID.'.<br><br> For BACuna Card corrections and concerns, please contact us at: <br>MITCS-Bacolod Landline: (034) 435-4168<br>Facebook Page: https://www.facebook.com/BCDGOVTECHSUPPORT<br>Email: bacunabcdts@gmail.com.<br><br><small><i>CONFIDENTIALITY NOTICE:</i> The contents of this email message and any attachments are intended solely for the addressee(s) and may contain confidential and/or privileged information and may be legally protected from disclosure. If you are not the intended recipient of this message or their agent, or if this message has been addressed to you in error, please immediately alert the sender by reply email and then delete this message and any attachments. If you are not the intended recipient, you are hereby notified that any use, dissemination, copying, or storage of this message or its attachments is strictly prohibited.</small><br><br><img src="cid:eoc" height="50" width="50">&nbsp;&nbsp;<img src="cid:covac" height="50" width="50">';

			    $mail->AltBody = 'Good day, Please generate your BACuna vaccination ID card through this link: https://vims.bacolodcity.gov.ph/main/bac_una_id_v2.php?vmID='.$genID.'. For BACuna Card corrections and concerns, please contact us at: MITCS-Bacolod Landline: (034) 435-4168, Facebook Page: https://www.facebook.com/BCDGOVTECHSUPPORT, Email: bacunabcdts@gmail.com. CONFIDENTIALITY NOTICE: The contents of this email message and any attachments are intended solely for the addressee(s) and may contain confidential and/or privileged information and may be legally protected from disclosure. If you are not the intended recipient of this message or their agent, or if this message has been addressed to you in error, please immediately alert the sender by reply email and then delete this message and any attachments. If you are not the intended recipient, you are hereby notified that any use, dissemination, copying, or storage of this message or its attachments is strictly prohibited.';

		    $mail->send();
		    echo 'Mail has been sent!';
		} catch (Exception $e) {
		    echo "Mail could not be sent.";
		}

	}else{
		echo "Error!";
	}
	

?>