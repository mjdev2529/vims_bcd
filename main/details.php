<?php
	session_start();
	if($_SESSION['in'] != 1){
		echo "<script>alert('Please login to continue...'); window.location.href='../';</script>";
	}
	include '../config/conn.php';

	if($_GET['vmID']){
		include '../config/conn.php';
		$vID = $_GET['vmID'];

		$vData = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_vaccination_vims WHERE vims_id = '$vID'"));
		$hasBooster = mysqli_num_rows(mysqli_query($conn,"SELECT vims_id FROM tbl_vaccination_vims WHERE first_name = '$vData[first_name]' AND last_name = '$vData[last_name]' AND birthday = '$vData[birthday]' AND dose_booster = 'Y'"));

		$fname = $vData["first_name"];
		$lname = $vData["last_name"];
		$bdate = $vData["birthday"];
?>
<!DOCTYPE html>
<html>
<head>
	<title>BACuna - Bacolod</title>
</head>
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/icons/css/all.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="../assets/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="../assets/css/fSelect.css">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="../assets/js/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/fSelect.js"></script>
<style type="text/css">
	body{
		padding-top: 5rem;
	}
</style>
<body>
	<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
	  <a class="navbar-brand" href="index.php">BACuna</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="index.php"><u>Home</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="no_email.php"><u>No Email</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="add_new.php"><u>Add New</u></a>
	      </li>
	      <li class="nav-item">
	      	<a class="nav-link text-primary" href="users.php"><u>User Management</u></a>
	      </li>
	    </ul>

	    <ul class="navbar-nav px-3">
		    <li class="nav-item text-nowrap">
		      <a class="nav-link" href="../ajax/logout.php"><i class="fa fa-sign-out-alt"></i> Sign out</a>
		    </li>
		</ul>
	  </div>
	</nav>
	<div class="container col-12 mb-3">
		<div class="col-12 mb-3" id="container">
			<h1>Vaccination Details for <u><?=$fname?> <?=$lname?></u></h1>
			<br>
			<?php if($hasBooster == 0){ ?>
				<div class="btn-group mb-3 col-1 offset-11">
			        <button class="btn btn-sm btn-outline-success" data-toggle="modal" data-target="#add_md">Add Booster</button>
			    </div>
			<?php }?>
			<table class="table table-bordered table-striped mt-2" id="tbl_new" style="text-align: center;">
		        <thead class="bg-success text-white">
					<tr>
						<th width="300px"></th>
						<th width="150px">QR UNIQUE ID</th>
						<th>LAST_NAME</th>
						<th>FIRST_NAME</th>
						<th>MIDDLE_NAME</th>
						<th>SUFFIX</th>
						<th>BARANGAY</th>
						<th>BIRTHDATE</th>
						<th>VACCINATION_DATE</th>
						<th>VACCINE_NAME</th>
						<th>VACCINATOR_NAME</th>
						<th>1ST_DOSE</th>
						<th>2ND_DOSE</th>
						<th>BOOSTER_DOSE</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="edit_md" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-edit"></i> Edit Data</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<div class="row">
				<div class="col-12 mb-3">
					<form id="edit_form" method="POST" action="" autocomplete="off">
						<div class="row">
							<div class="col-6 mb-2">
								<label>Last Name</label>
								<input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" required="">
								<input type="hidden" class="form-control" name="vID" id="vID">
							</div>
							<div class="col-6 mb-2">
								<label>First Name</label>
								<input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" required="">
							</div>
							<div class="col-6 mb-2">
								<label>Middle Name</label>
								<input type="text" class="form-control" name="mname" id="mname" placeholder="Middle Name">
							</div>
							<div class="col-6 mb-2">
								<label>Suffix</label>
								<input type="text" class="form-control" name="suffix" id="suffix" placeholder="Suffix">
							</div>
							<div class="col-6 mb-2">
								<label>Barangay</label>
								<input type="text" class="form-control" name="brgy" id="brgy" placeholder="Barangay" required="">
							</div>
							<div class="col-6 mb-2">
								<label>Birth Date</label>
								<input type="date" class="form-control" name="bdate" id="bdate" placeholder="Birth Date" required="">
							</div>

							<div class="col-6 mt-3">
								<div class="col-10 offset-1 mb-2">
									<label><b>Vaccination Details ( <span id="dose"></span> )</b></label><br>
									<label>Vaccination Date</label>
									<input type="date" class="form-control" name="vdate1" id="vdate1" placeholder="Vaccination Date" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccine Name</label>
									<input type="text" class="form-control" name="vname1" id="vname1" placeholder="Vaccine Name" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>LOT Number</label>
									<input type="text" class="form-control" name="lotnum1" id="lotnum1" placeholder="LOT Number" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccinator Name</label>
									<input type="text" class="form-control" name="vrname1" id="vrname1" placeholder="Vaccinator Name" required="">
									<input type="hidden" name="dose_1" value="Y">
								</div>
							</div>
							
							<div class="col-4 offset-8">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						    	<button type="submit" class="btn btn-primary">Save</button>
						    </div>
						</div>
					</form>
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="add_md" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-edit"></i> Edit Data</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<div class="row">
				<div class="col-12 mb-3">
					<form id="add_form" method="POST" action="" autocomplete="off">
						<div class="row">
							<div class="col-6 mb-2">
								<label>Last Name</label>
								<input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" required="" value="<?=$lname?>">
								<input type="hidden" class="form-control" name="vID" id="vID" value="booster">
							</div>
							<div class="col-6 mb-2">
								<label>First Name</label>
								<input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" required="" value="<?=$fname?>">
							</div>
							<div class="col-6 mb-2">
								<label>Middle Name</label>
								<input type="text" class="form-control" name="mname" id="mname" placeholder="Middle Name" value="<?=$vData['middle_name']?>">
							</div>
							<div class="col-6 mb-2">
								<label>Suffix</label>
								<input type="text" class="form-control" name="suffix" id="suffix" placeholder="Suffix" value="<?=$vData['suffix']?>">
							</div>
							<div class="col-6 mb-2">
								<label>Barangay</label>
								<input type="text" class="form-control" name="brgy" id="brgy" placeholder="Barangay" required="" value="<?=$vData['barangay']?>">
							</div>
							<div class="col-6 mb-2">
								<label>Birth Date</label>
								<input type="date" class="form-control" name="bdate" id="bdate" placeholder="Birth Date" required="" value="<?=$vData['birthday']?>">
							</div>

							<div class="col-6 mt-3">
								<div class="col-10 offset-1 mb-2">
									<label><b>Vaccination Details ( Booster )</b></label><br>
									<label>Vaccination Date</label>
									<input type="date" class="form-control" name="vdate1" id="vdate1" placeholder="Vaccination Date" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccine Name</label>
									<input type="text" class="form-control" name="vname1" id="vname1" placeholder="Vaccine Name" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>LOT Number</label>
									<input type="text" class="form-control" name="lotnum1" id="lotnum1" placeholder="LOT Number" required="">
								</div>
								<div class="col-10 offset-1 mb-2">
									<label>Vaccinator Name</label>
									<input type="text" class="form-control" name="vrname1" id="vrname1" placeholder="Vaccinator Name" required="">
									<input type="hidden" name="dose_1" value="Y">
								</div>
							</div>
							
							<div class="col-4 offset-8">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						    	<button type="submit" class="btn btn-primary">Save</button>
						    </div>
						</div>
					</form>
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>

</body>
</html>
<script type="text/javascript">
	$(document).ready( function(){
		tbl_new();
	});

	function tbl_new(){
		$("#tbl_new").DataTable().destroy();
		$("#tbl_new").DataTable({
	      "processing": true,
	      "ajax":{
	        "type":"POST",
	        "url":"../ajax/datatables/new_data.php",
	        "dataSrc": "data",
	        "data": {view: 3, fname: "<?=$fname?>", lname: "<?=$lname?>", bdate: "<?=$bdate?>"}
	      },
	      "columns": [
	        {"mRender": function(data,type,row){
	      		return "<button class='btn btn-outline-primary btn-block' onclick='user_edit("+row.vmaster_id+")'><i class='fa fa-edit'></i>Edit</button>"+
	      		"<button class='btn btn-outline-danger btn-block' onclick='user_delete("+row.vmaster_id+")'><i class='fa fa-trash'></i>Delete</button>";
	      	}},
	      	{"data": "qrid"},
	        {"data": "last_name"},
	        {"data": "first_name"},
	        {"data": "middle_name"},
	        {"data": "suffix"},
	        {"data": "barangay"},
	        {"data": "birthdate"},
	        {"data": "vaccination_date"},
	        {"data": "vaccine_manufacturer_name"},
	        {"data": "vaccinator_name"},
	        {"data": "dose_1"},
	        {"data": "dose_2"},
	        {"data": "dose_booster"}
	      ]
		});
	}

	function user_delete(vID){
		var conf = confirm("Are you sure to delete data?");
		if(conf){
			$(".btn").prop("disabled", true);
		    $.ajax({
		        url: "../ajax/new_delete.php",
		        data: {vID: vID},
		        type: 'POST',
		        success: function (data) {
		           if(data == 1){
		           	alert("Success: Data was removed.");
		           	$("input").val("");
		           }else{
		           	alert("Error: Something was wrong.");
		           }
					$(".btn").prop("disabled", false);
					tbl_new();
					window.location.reload();
		        }
		    });
		}
	}

	function user_edit(vID){
		$.ajax({
	        url: "../ajax/new_details.php",
	        data: {vID: vID},
	        type: 'POST',
	        success: function (data) {
	           if(data != ""){
	           	var o = JSON.parse(data);
				$("#edit_md").modal();
				$("#vID").val(o.vmaster_id);
				$("#lname").val(o.last_name);
				$("#fname").val(o.first_name);
				$("#mname").val(o.middle_name);
				$("#suffix").val(o.suffix);
				$("#brgy").val(o.barangay);
				$("#bdate").val(o.birthdate);

				$("#vname1").val(o.vname1);
				$("#lotnum1").val(o.lotnum1);
				$("#vrname1").val(o.vrname1);
				$("#vdate1").val(o.vdate1);
				$("#dose").html(o.dose);

	           }else{
	           	alert("Error: Something was wrong.");
	           }
	        }
	    });
	}

	$("#edit_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$(".btn").prop("disabled", true);
	    $.ajax({
	        url: "../ajax/new_update.php",
	        data: data,
	        type: 'POST',
	        success: function (data) {
	           if(data == 1){
	           	alert("Success: Data was updated.");
	           	$("input").val("");
	           }else{
	           	alert("Error: Something was wrong.");
	           }
				$(".btn").prop("disabled", false);
				tbl_new();
				$("#edit_md").modal('hide');
	        }
	    });
	});

	$("#add_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$(".btn").prop("disabled", true);
	    $.ajax({
	        url: "../ajax/new_update.php",
	        data: data,
	        type: 'POST',
	        success: function (data) {
	           if(data == 1){
	           	alert("Success: Data was updated.");
	           	$("input").val("");
	           }else{
	           	alert("Error: Something was wrong.");
	           }
				$(".btn").prop("disabled", false);
				tbl_new();
				$("#add_md").modal('hide');
				window.location.reload();
	        }
	    });
	});

</script>
<?php } ?>